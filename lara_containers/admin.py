"""_____________________________________________________________________

:PROJECT: LARA

*lara_containers admin *

:details: lara_containers admin module admin backend configuration.
         - 

:file:    admin.py
:authors: mark doerr

:date: (creation)          20180624
:date: (last modification) 20180625

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.contrib import admin

from .models import ContainerClass, Container

class ContainerClassAdmin(admin.ModelAdmin):
    list_display = ('name','description')
    list_filter = ['name']
    search_fields = ['name']
    
#~ class DeviceInline(admin.TabularInline):
    #~ model = Lara_group
    
class ContainerAdmin(admin.ModelAdmin):
    #~ fieldsets = [
        #~ (None,               {'fields': ['question_text']}),
        #~ ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    #~ ]
    #~ inlines = [DeviceInline]
     
    #~ list_display = ('name', 'full_name', 'location', 'serial_no', 'fabrication_date', 'purchase_date', 'end_of_warrenty_date', 'warrentyLeft', 'inWarrentyPeriode' )
    list_display = ('name', 'serial_no' )
    list_filter = ['container_class']
    search_fields = ['name']
    
admin.site.register(ContainerClass, ContainerClassAdmin)
admin.site.register(Container, ContainerAdmin)

